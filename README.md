# Documentation #

- [RU](https://bitbucket.org/eroller/doc/overview#mark-down-header-ru)
- [EN](https://bitbucket.org/eroller/doc/overview#mark-down-header-en)

#RU

Документация по версиям:

* **v1.0**
	* [Т.З.](https://bitbucket.org/eroller/doc/src/master/v1.0/spec-v1.0-ru.md)
	* [Протокол]()
	* [Графика](https://bitbucket.org/eroller/doc/src/master/v1.0/ui-v1.0-ru.md)
	* [Модель](https://bitbucket.org/eroller/doc/src/master/v1.0/model-v1.0-ru.md)

Все вопросы, связанные с документации можно обсудить создав соответствующую [Issue](https://bitbucket.org/eroller/doc/issues?status=new&status=open)

#EN

We terribly sorry but currently we have no spec on english. If you want to help project contact me @gviryaskin or create an [Issue](https://bitbucket.org/eroller/doc/issues?status=new&status=open)